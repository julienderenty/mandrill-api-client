package controllers;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Application extends Controller {

    public static Result index() {
        return ok(views.html.index.render("Your new application is ready."));
    }


    public static Result submit() {

        DynamicForm form = Form.form().bindFromRequest();
        String mandrill = form.data().get("mandrill");
        String input = form.data().get("input");
        String subject = form.data().get("subject");
        String fromEmail = form.data().get("fromEmail");
        String fromName = form.data().get("fromName");
        String body = form.data().get("body");

        String[] emails = input.split("\\n");

        Logger.info("Found " +emails.length + " emails.  Sending...");



        MandrillApi mandrillApi = new MandrillApi("mandrill");

        // create your message
        MandrillMessage message = new MandrillMessage();
        message.setSubject(subject);
        message.setHtml(body);
        message.setAutoText(true);
        message.setFromEmail(fromEmail);
        message.setFromName(fromName);
        List<MandrillMessage.RecipientMetadata> list=new ArrayList<MandrillMessage.RecipientMetadata>();

        // set per recipient meta data
        for(String email : emails) {
            MandrillMessage.RecipientMetadata meta = new MandrillMessage.RecipientMetadata();
            meta.setRcpt(email);
            Map<String, String> map = new HashMap<>();
            map.put("email", email);
            meta.setValues(map);
            list.add(meta);
            message.setRecipientMetadata(list);
        }


        // add recipients
        ArrayList<MandrillMessage.Recipient> recipients = new ArrayList<MandrillMessage.Recipient>();
        for(String email : emails) {
            MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(email);
            recipients.add(recipient);
        }

        message.setTo(recipients);

        message.setPreserveRecipients(false);
        ArrayList<String> tags = new ArrayList<String>();
        tags.add("test");
        tags.add("helloworld");
        message.setTags(tags);


        try {
            MandrillMessageStatus[] messageStatusReports = mandrillApi
                    .messages().send(message, true);

            if (messageStatusReports.length > 0) {
                Logger.info(messageStatusReports.length + " " + messageStatusReports[0].getStatus());
            }
            return ok(messageStatusReports.length + " " + messageStatusReports[0].getStatus());

        } catch (MandrillApiError mandrillApiError) {
            mandrillApiError.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ok("Error");
    }

}
